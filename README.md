# Тестовое задание:

Данные хранятся в 3 таблицах в разных форматах:

- Данные кабинета рекламной сети - network.csv
- Сырые данные трекера - tracker.db
- Данные выгрузки CRM - [Google spreadsheet](https://docs.google.com/spreadsheets/d/1Ix2XlG3pjnfJjpWVPTqPfdD64kF5z86Z-2M3JcrcjLw/edit?usp=sharing)

Данные из CRM имеют нестандартизированные названия стран.
###### Необходимо:
1. Найти способ приведения названия стран к [стандарту](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2#Officially_assigned_code_elements)
2. Составить сводную таблицу из данных трекера в разрезе: date, campaign, campaign_id, country_code
с подсчетом количества событий в каждом из срезов при помощи SQL запроса.
3. Корректно объединить данные из 3х источников по возможным ключам.
4. Рассчитать метрики эффективности например:
    * CPC (Cost / Clicks)
    * IR (Installs / Clicks)
    * CR (FTD_MOB / install)
    * CPO (Сost / Dep)
    * ROI ((IN - OUT) / Cost)
5. Составить отчет по рекламным кампаниям за период 2020-08-01 - 2020-08-30.
Дать оценку эффективности рекламных кампаний.
Выделить основные признаки влияющие на данные метрики:
    * ROI
    * CPO
    * Cost
6. Сделать прогноз по текущим кампаниям(период 2020-09-01 - 2020-09-15) и написать почему вы выбрали данный подход для построения прогноза.
7. Используя библиотеку python-pptx или ее аналог подготовить аналитический отчёт со срезами и выводами.

Результатом выполненного задания является .py скрипт или .ipynb выложенный в git.

###### Примечание:
В данных множество подводных камней, просьба подойти к анализу ответственно, корректность отчёта будет учитываться в оценке работы.

